FROM almir/webhook
RUN apk add curl
COPY hooks/ /etc/webhook/
RUN chmod +x /etc/webhook/webapp/*.sh
CMD ["-verbose", "-hooks=/etc/webhook/hooks.json", "-hotreload"]
