#!/bin/sh

#get keptn endpoint
KEPTN_ENDPOINT=http://my-keptn.demo/api



BRANCH=${GIT_REVISION%/*}
COMMIT_SHA=${GIT_REVISION#*/}
#BRANCH="main"
#COMMIT_SHA="123"

echo '========================================================'
echo 'branch:'  $BRANCH
echo 'commit:' $COMMIT_SHA
echo '========================================================'

generate_post_data()
{
cat <<EOF
{
    "contenttype": "application/json",
    "data":
    {
        "project": "keptn-webhook",
        "service": "animals",
        "stage": "dev",
        "configurationChange": {},
        "git_data":{
            "branch": "$BRANCH",
            "commit_sha": "$COMMIT_SHA"
        }
    },
    "source": "flux",
    "specversion": "1.0",
    "type": "sh.keptn.event.dev.delivery.triggered"
}
EOF
}

# trigger delivery
curl -X POST "$KEPTN_ENDPOINT/v1/event" \
-H "accept: application/json; charset=utf-8" \
-H "x-token: $KEPTN_API_TOKEN" \
-H "Content-Type: application/json; charset=utf-8" \
-d "$(generate_post_data)"
